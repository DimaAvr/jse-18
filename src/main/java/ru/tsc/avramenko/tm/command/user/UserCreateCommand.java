package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserCreateCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create new user.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().create(login, password, email);
    }

}