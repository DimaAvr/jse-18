package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

}