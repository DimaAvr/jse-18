package ru.tsc.avramenko.tm.command.system;

import ru.tsc.avramenko.tm.command.AbstractCommand;

public class AboutDisplayCommand extends AbstractCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer information.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Avramenko Dmitry");
        System.out.println("E-MAIL: DimaAvramenko@yandex.ru");
    }

}