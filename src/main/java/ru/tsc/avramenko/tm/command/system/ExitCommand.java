package ru.tsc.avramenko.tm.command.system;

import ru.tsc.avramenko.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}