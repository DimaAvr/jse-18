package ru.tsc.avramenko.tm.command.project;

import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishById(id);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().finishById(id);
        System.out.println("[OK]");
    }

}