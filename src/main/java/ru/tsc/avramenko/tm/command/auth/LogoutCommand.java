package ru.tsc.avramenko.tm.command.auth;

import ru.tsc.avramenko.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "User logout from system.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}