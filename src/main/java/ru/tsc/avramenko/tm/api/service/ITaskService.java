package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    Task changeStatusById(String id, Status status);

    Task changeStatusByName(String name, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task startById(String id);

    Task startByName(String name);

    Task startByIndex(Integer index);

    Task finishById(String id);

    Task finishByName(String name);

    Task finishByIndex(Integer index);

    void clear();

}