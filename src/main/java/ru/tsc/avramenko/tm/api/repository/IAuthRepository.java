package ru.tsc.avramenko.tm.api.repository;

public interface IAuthRepository {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId);

}