package ru.tsc.avramenko.tm.component;

import ru.tsc.avramenko.tm.api.repository.*;
import ru.tsc.avramenko.tm.api.service.*;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import ru.tsc.avramenko.tm.command.auth.LoginCommand;
import ru.tsc.avramenko.tm.command.auth.LogoutCommand;
import ru.tsc.avramenko.tm.command.project.*;
import ru.tsc.avramenko.tm.command.system.*;
import ru.tsc.avramenko.tm.command.task.*;
import ru.tsc.avramenko.tm.command.user.*;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.exception.system.UnknownCommandException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.repository.*;
import ru.tsc.avramenko.tm.service.*;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(authRepository, userService);


    public void start(String[] args) {
        displayWelcome();
        initData();
        initUsers();
        parseArgs(args);
        process();
    }

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());
        registry(new ArgumentsDisplayCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectShowListCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new LoginCommand());
        registry(new LogoutCommand());

        registry(new UserShowListCommand());
        registry(new UserCreateCommand());
        registry(new UserClearCommand());
        registry(new UserChangeRoleCommand());
        registry(new UserShowByIdCommand());
        registry(new UserShowByLoginCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserUpdateByIdCommand());
        registry(new UserUpdateByLoginCommand());
    }

    private void initData() {
        projectService.add(new Project("Project C", "1"));
        projectService.add(new Project("Project A", "2"));
        projectService.add(new Project("Project B", "3"));
        projectService.add(new Project("Project D", "4"));
        taskService.add(new Task("Task C", "1"));
        taskService.add(new Task("Task A", "2"));
        taskService.add(new Task("Task B", "3"));
        taskService.add(new Task("Task D", "4"));
        projectService.finishByName("Project B");
        projectService.startByName("Project A");
        taskService.finishByName("Task C");
        taskService.startByName("Task B");
    }

    public void initUsers() {
        userService.create("Test", "Test", Role.USER);
        userService.updateUserByLogin("Test", "Testoviy", "Test", "Testovich", "test@gmail.com");
        userService.create("Admin", "Admin", Role.ADMIN);
        userService.updateUserByLogin("Admin", "Adminov", "Admin", "Adminovich", "admin@gmail.com");
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void displayAuth() {
        System.out.println("You are not authorized. " + "Enter one of the following commands: " + "\n" + "\n"
                + commandService.getCommandByName("login") + "\n"
                + commandService.getCommandByName("exit") + "\n"
                + commandService.getCommandByName("user-create") + "\n"
                + commandService.getCommandByName("about") + "\n"
                + commandService.getCommandByName("version") + "\n");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitCommand().name();
        String command = "";
        while (!exit.equals(command)) {
            try {
                while (!getAuthService().isAuth()) { //работа с неаутентифицированными пользователями
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit": parseCommand("exit"); break;
                            case "login": parseCommand("login"); break;
                            case "user-create": parseCommand("user-create"); break;
                            case "about": parseCommand("about"); break;
                            case "version": parseCommand("version"); break;
                            default: throw new AccessDeniedException();
                        }
                    } catch (final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    public void parseArg(final String arg) {
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (arguments == null) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}